---
name: Bug
about: Various bugs and suggestions for improvement

---

### Describe the bug*


### To Reproduce (sequencing; on what page)
1. 
2. 

### Expected behavior


### What do you use: what device, model, OS, browser, extensions


### Screenshots
